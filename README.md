# README #

This docker image integrates  [the Dynamic DNS client of DNS-Exit](http://www.dnsexit.com/Direct.sv?cmd=userIpClients) with HA-Proxy.

The purpose is to have an [HAProxy](http://www.haproxy.org/) instance answering outside workd from a dynamic changing IP address.

Both the HAProxy daemon and the Dynamic DNS client are monitored and managed by [Supervisor](http://supervisord.org/).

### How do I set up the dynamic IP update? ###

1. If not already registered sign up for your account at [DNS-Exit](http://www.dnsexit.com/) and configure a domain for dynamic DNS updates
2. Launch a container with this image and get a bash shell inside it
2. Go to the install dir of "ipUpdate":
  * `# cd /ipUpdate-1.6/`
3. run the setup script and insert the credentials of your DNS-Exit account:
  * `# ./setup.pl`
4. Select a previously configured domain at the prompt:
  * *Please select your domains (if you want to select more than one domain, please separete them by space):* `your_selection_here`
5. **Important!**: answer **no** to the daemon configuration:
  * *Do you want to run it as a daemon?* **no**
6. Either wait about ten minutes for the updating of the IP to be triggered or restart the container to make the configuration work

### Bound mounting of configuration files ###

* **HAProxy**: `haproxy.cfg` is placed in the root directory, you may for example:
  * `--volume` *your_host_path*`:/haproxy.cfg`
* **ipUpdate**: `dnsexit.conf` is placed under `/etc`, you may for example:
  * `--volume` *your_host_path*`:/etc/dnsexit.conf`

### Ports Exposed ###

* Supervisord web server:
  * `EXPOSE 8001`
* http ports managed by HAProxy:
  * `EXPOSE 80 443`
* HAProxy stats
  * `EXPOSE 1936`

### Exemplary docker-compose.yml ###
```
dynamic-haproxy:
  image: antoniorossi/dynamic-haproxy
  container_name: haproxy
  hostname: haproxy
  ports:
    - "80:80"
    - "443:443"
    - "1936:1936"
    - "8001:8001"
  volumes:
    - /your/host/path/haproxy.cfg:/haproxy.cfg
    - /your/host/path/dnsexit.conf:/etc/dnsexit.conf
```
