#!/bin/bash
# run_once.sh

# config supervisord runtime
sed -i -- "s/username = user/username = $USER/g" /run.conf
sed -i -- "s/password = 123/password = $PASSWORD/g" /run.conf

# generate ssl keys
openssl genrsa -out /etc/ssl/private/server.key 2048
mkdir /etc/ssl/csr
openssl req -new -key /etc/ssl/private/server.key -out /etc/ssl/csr/server.csr -subj "/C=CO/ST=$STATE/L=$LOCATION/O=$ORGANIZATION/OU=$UNIT/CN=$COMMON_NAME"
openssl x509 -req -days 365 -in /etc/ssl/csr/server.csr -signkey /etc/ssl/private/server.key -out /etc/ssl/certs/server.crt
cat /etc/ssl/certs/server.crt /etc/ssl/private/server.key > /etc/ssl/certs/server.bundle.pem

# Change supervisor config file
rm /etc/supervisor/conf.d/install.conf
mv /run.conf /etc/supervisor/conf.d/
supervisorctl update
supervisorctl remove run_once
supervisorctl reread
